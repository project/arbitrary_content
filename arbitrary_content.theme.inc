<?php
// $Id$

/**
 * @file
 * All theme functions exist here.
 *
 */

function theme_arbitrary_content_file_widget_preview($item) {
  // Remove the current description so that we get the filename as the link.
  if (isset($item['data']['description'])) {
    unset($item['data']['description']);
  }

  return '<div class="filefield-file-info">'.
           '<div class="filename">'. theme('arbitrary_content_file', $item) .'</div>'.
           '<div class="filesize">'. format_size($item['filesize']) .'</div>'.
           '<div class="filemime">'. $item['filemime'] .'</div>'.
         '</div>';
}

function theme_arbitrary_content_file($file) {
  // Views may call this function with a NULL value, return an empty string.
  if (empty($file['fid'])) {
    return '';
  }

  $path = $file['filepath'];
  $url = file_create_url($path);
  $icon = theme('arbitrary_content_file_icon', $file);

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  // TODO: Possibly move to until I move to the more complex format described
  // at http://darrelopry.com/story/microformats-and-media-rfc-if-you-js-or-css
  $options = array(
    'attributes' => array(
      'type' => $file['filemime'] . '; length=' . $file['filesize'],
    ),
  );

  // Use the description as the link text if available.
  if (empty($file['data']['description'])) {
    $link_text = $file['filename'];
  }
  else {
    $link_text = $file['data']['description'];
    $options['attributes']['title'] = $file['filename'];
  }

  return '<div class="filefield-file">'. $icon . l($link_text, $url, $options) .'</div>';
}

function theme_arbitrary_content_file_icon($file) {
  $mpath = drupal_get_path('module', 'arbitrary_content');
  $icon_url = $mpath . '/file-icon.png';
  if (is_object($file)) {
    $file = (array) $file;
  }
  $mime = check_plain($file['filemime']);

  $dashed_mime = strtr($mime, array('/' => '-', '+' => '-'));

  $image = theme('image', $icon_url, $mime . ' icon', '', array('class' => 'arbitrary-content-file file-icon-' . $dashed_mime));

  return $image;
}

function theme_arbitrary_content_field($field) {
    switch ($field['field_type']) {
    case 'file':
        return '<div class="ac_' . $field['field_name'] . '">' . theme('arbitrary_content_file', (array)$field['file']) . '</div>';
    case 'textfield':
        return '<div class="ac_' . $field['field_name'] . '">' . check_plain($field['value']) . '</div>';
    case 'textarea':
        if (empty($field['data']['format']))
            $format = FILTER_FORMAT_DEFAULT;
        else
            $format = $field['data']['format'];

        return '<div class="ac_' . $field['field_name'] . '">' . check_markup($field['value'], $format, FALSE) . '</div>';
    }
}