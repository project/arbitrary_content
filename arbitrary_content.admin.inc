<?php
// $Id$

/**
 * @file
 * All administration functions, forms, menu hooks.
 */

function arbitrary_content_administer() {
  ctools_include('export');

  $content_items = ctools_export_crud_load_all('arbitrary_content');

  $header = array(t('Name'), t('Description'), t('Type'), array('data' => t('Operations'), 'colspan' => '4'));
  $rows = array();
  foreach ($content_items as $ci) {
    if ($ci->type == 'Default') {
      $delete = 'Default';
    }
    else if ($ci->type == 'Overridden'){
      $delete = l(t('Revert'), 'admin/build/ac/' . $ci->name . '/delete');
    }
    else {
      $delete = l(t('Delete'), 'admin/build/ac/' . $ci->name . '/delete');
    }

    if ($ci->fields) {
      $field_content = l(t('Edit Field Content'), 'admin/build/ac/' . $ci->name . '/edit_field_data');
    }
    else {
      $field_content = 'Edit Field Content';
    }
    $rows[] = array(
      $ci->name,
      $ci->description,
      $ci->ac_type,
      l(t('Edit'),            'admin/build/ac/edit_ac/' . $ci->name),
      l(t('Fields'),     'admin/build/ac/' . $ci->name . '/fields'),
      $field_content,
      $delete,
    );
  }

  return theme('table', $header, $rows);
}

function arbitrary_content_add_form() {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Content name'),
    '#description' => t('This is the unique name of the Content. It must contain only alphanumeric or underscores. It is used to identify the content internally, to generate unique template names and URL paths.'),
    '#required' => TRUE,
    '#maxlength' => 32,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Content description'),
    '#description' => t('This description will appear on the administrative UI.'),
  );

  $form['ac-type'] = array(
    '#type' => 'radios',
    '#title' => t('Content type'),
    '#description' => t('The type is how the content will expose itself. Either as a block or a page.'),
    '#options' => array('Block', 'Page'),
    '#default_value' => 0
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

function arbitrary_content_add_form_validate($form, &$form_state) {
  $name = $form_state['values']['name'];

  // Content name must be alphanumeric or underscores.
  if (preg_match('/[^a-zA-Z0-9_]/', $name))
    form_error($form['name'], t('The name must only use alphanumeric or underscores.'));

  // Content name must be unique.

  $content = arbitrary_content_load($form_state['values']['name']);

  if ($content)
    form_error($form['name'], t('The name must be unique.'));
}

function arbitrary_content_add_form_submit($form, &$form_state) {
  $content = (object) NULL;

  $content->ac_type = $form_state['values']['ac-type'] ? 'page' : 'block';
  $content->name = $form_state['values']['name'];
  $content->description = $form_state['values']['description'];
  $content->export_type = NULL;
  arbitrary_content_save_content($content);
  drupal_rebuild_theme_registry();
  drupal_goto('admin/build/ac/list');
}

function arbitrary_content_edit_form(&$form_state, $ac) {
  $form_state['ac'] = $ac;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Content name'),
    '#required' => TRUE,
    '#attributes' => array('readonly' => 'readonly'),
    '#default_value' => $ac->name,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Content description'),
    '#description' => t('This description will appear on the administrative UI.'),
    '#default_value' => $ac->description,
  );

  $form['ac-type'] = array(
    '#type' => 'textfield',
    '#title' => t('Content type'),
    '#default_value' => ($ac->ac_type == 'block') ? 'Block' : 'Page',
    '#attributes' => array('readonly' => 'readonly'),
  );

  if ($ac->ac_type == 'page') {
    $form['page-path'] = array(
      '#type' => 'textfield',
      '#title' => t('Page Path'),
      '#default_value' => $ac->data['path'],
    );
    $form['page-title'] = array(
      '#type' => 'textfield',
      '#title' => t('Page Title'),
      '#default_value' => $ac->data['title'],
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

function arbitrary_content_edit_form_validate($form, &$form_state) {
  $ac = $form_state['ac'];
  if ($ac->ac_type == 'page') {
    $path = $form_state['values']['page-path'];
    if (preg_match('/[^a-zA-Z0-9_\/\-]/', $path)) {
      form_error($form['name'], t('The name must only use alphanumeric, underscores, forward slashes and dashes.'));
    }
  }
}

function arbitrary_content_edit_form_submit($form, &$form_state) {
  $ac = $form_state['ac'];

  $ac->description = $form_state['values']['description'];
  $ac->data = array(
    'path' => $form_state['values']['page-path'],
    'title' => $form_state['values']['page-title']
  );

  arbitrary_content_save_content($ac);

  if ($ac->ac_type == 'page')
    menu_rebuild();
}

function arbitrary_content_fields_page($ac) {
  $output = '';
  $fields = $ac->fields;

  $header = array(t('Name'), t('Description'), t('Type'), array('data' => t('Operations'), 'colspan' => '2'));
  $rows = array();
  if ($fields) {
    foreach ($fields as $f) {
      $rows[] = array(
        $f->field_name,
        $f->description,
        $f->field_type,
        l(t('Edit Field'),   'admin/build/ac/' . $ac->name . '/edit_field/' . $f->field_name),
        l(t('Delete Field'), 'admin/build/ac/' . $ac->name . '/delete_field/' . $f->field_name),
      );
    }
  }
  $output .= theme('table', $header, $rows);
  $output .= l(t('Add Field'), 'admin/build/ac/' . $ac->name . '/add_field');

  return $output;
}

function arbitrary_content_edit_field_data_form(&$form_state, $ac) {
  $form_state['ac'] = $ac;
  $fields = $ac->fields;

  if($fields) {
    foreach ($fields as $field) {
      if ($field->field_type == 'textfield') {
        $value = arbitrary_content_load_field_data($field);
        $form[$field->field_name] = array(
          '#title' => $field->description,
          '#type' => $field->field_type,
          '#default_value' => $value->value,
        );
      }
      elseif ($field->field_type == 'file') {
        $field_value = arbitrary_content_load_field_data($field);
        $form['#attributes'] = array('enctype' => "multipart/form-data");
        $form[$field->field_name] = array(
          '#type' => 'fieldset',
          '#title' => t($field->description)
        );

        if ($field_value->value) {
          $file = db_fetch_object(db_query('SELECT f.* FROM {files} f WHERE f.fid = %d', $field_value->value));
          if ($file) {
            $form[$field->field_name]['original'] = array(
              '#value' => theme('arbitrary_content_file_widget_preview', (array)$file),
            );
            $form[$field->field_name]['remove-' . $field->field_name] = array(
              '#type' => 'checkbox',
              '#title' => 'Delete?'
            );
          }
        }
        else {
          $form[$field->field_name]['file-' . $field->field_name] = array(
            '#title' => $field->description,
            '#type' => $field->field_type,
          );
        }
      }
      elseif ($field->field_type == 'textarea') {
        $fieldset = $field->field_name . '-fieldset';
        $field_format = $field->field_name . '-format';

        $form[$fieldset] = array(
          '#type' => 'fieldset',
          '#title' => t($field->description)
        );
        $value = arbitrary_content_load_field_data($field);
        $form[$fieldset][$field->field_name] = array(
          '#type' => $field->field_type,
          '#default_value' => $value->value,
        );
        if (empty($value->data['format']))
          $format = FILTER_FORMAT_DEFAULT;
        else
          $format = $value->data['format'];

        $form[$fieldset]['format'] = filter_form($format, NULL , array($field_format));
      }
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }

  return $form;
}

function arbitrary_content_edit_field_data_form_validate($form, &$form_state) {
  $ac = $form_state['ac'];
  $fields = $ac->fields;

  foreach ($fields as $field) {
    if ($field->field_type == 'file') {
      $field_name = $field->field_name;
      if (isset($_FILES['files']) && is_uploaded_file($_FILES['files']['tmp_name']['file-' . $field_name])) {
        // attempt to save the uploaded file
        $file = file_save_upload('file-' . $field_name, array(), file_directory_path());

        // set error if file was not uploaded
        if (!$file) {
          drupal_set_message(t('No file was uploaded'));
        }
        else {
          // set files to form_state, to process when form is submitted
          file_set_status($file, FILE_STATUS_PERMANENT);
          $form_state['values'][$field_name] = $file->fid;
        }
      }
    }
  }
}

function arbitrary_content_edit_field_data_form_submit($form, &$form_state) {
  $ac = $form_state['ac'];
  $fields = $ac->fields;

  foreach ($fields as $field) {
    if ($field->field_type == 'file' && isset($form_state['values']['remove-' . $field->field_name])) {
      if ($form_state['values']['remove-' . $field->field_name]) {
        $field_value = arbitrary_content_load_field_data($field);
        $file = db_fetch_object(db_query('SELECT f.* FROM {files} f WHERE f.fid = %d', $field_value));
        file_set_status($file, FILE_STATUS_TEMPORARY);
        arbitrary_content_save_field_data($field, NULL);
      }
    }
    elseif ($field->field_type == 'textarea') {
      $value = $form_state['values'][$field->field_name];
      $data = array('format' => $form_state['values'][$field->field_name . '-format']);
      arbitrary_content_save_field_data($field, $value, $data);
    }
    else {
      $value = $form_state['values'][$field->field_name];
      arbitrary_content_save_field_data($field, $value);
    }

  }

}

function arbitrary_content_add_field_form(&$form_state, $ac) {
  $form_state['ac'] = $ac;

  $form['field-name'] = array(
    '#type' => 'textfield',
    '#title' => t('Field name'),
    '#description' => t('This is the unique name of the Field. It must contain only alphanumeric characters and underscores.'),
    '#required' => TRUE,
    '#maxlength' => 32,
  );

  $form['field-type'] = array(
    '#type' => 'select',
    '#title' => t('Field type'),
    '#description' => t('The type of field for the user to interact with.'),
    '#options' => array(
      'textfield' => 'Text Field',
      'textarea' => 'Text Area',
      'file' => 'File Field'),
    '#default_value' => 'textfield'
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Field description'),
    '#description' => t('This description will appear on the field data edit UI.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

function arbitrary_content_add_field_form_validate($form, &$form_state) {
  $field_name = $form_state['values']['field-name'];
  $ac = $form_state['ac'];

  // Content name must be alphanumeric or underscores.
  if (preg_match('/[^a-zA-Z0-9_\-]/', $field_name)) {
    form_error($form['field-name'], t('The name must only use alphanumeric or underscores.'));
  }

  // Content name must be unique.

  $field = arbitrary_content_load_field($ac, $form_state['values']['field-name']);
  if ($field) {
    form_error($form['field-name'], t('The name must be unique.'));
  }
}

function arbitrary_content_add_field_form_submit($form, &$form_state) {
  $ac = $form_state['ac'];

  $field = (object) NULL;
  $field->field_type = $form_state['values']['field-type'];
  $field->field_name = $form_state['values']['field-name'];
  $field->description = $form_state['values']['description'];
  $field->acname = $ac->name;
  $field->acid = $ac->acid;

  $ac->fields[] = $field;

  arbitrary_content_save_content($ac);

  drupal_goto('admin/build/ac/edit_fields/' .  $ac->acid);
}

function arbitrary_content_edit_field_form(&$form_state, $field) {
  $form_state['field'] = $field;

  $form['field-name'] = array(
    '#type' => 'textfield',
    '#title' => t('Field name'),
    '#description' => t('This is the unique name of the Field. It must contain only alphanumeric characters and underscores.'),
    '#maxlength' => 32,
    '#default_value' => $field->field_name,
    '#disabled' => TRUE,
  );

  $form['field-type'] = array(
    '#type' => 'select',
    '#title' => t('Field type'),
    '#description' => t('The type of field for the user to interact with.'),
    '#options' => array(
      'textfield' => 'Text Field',
      'textarea' => 'Text Area',
      'file' => 'File Field'),
    '#default_value' => $field->field_type,
    '#disabled' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Field description'),
    '#description' => t('This description will appear on the field data edit UI.'),
    '#default_value' => $field->description
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function arbitrary_content_edit_field_form_submit($form, &$form_state) {
  $field = $form_state['field'];
  $ac = arbitrary_content_load($field->acname);
  $field_name = $field->field_name;

  if (!empty($form_state['values']['description'])) {
    foreach ($ac->fields as $key => $field) {
      if ($field->field_name == $field_name) {
        $ac->fields[$key]->description = $form_state['values']['description'];
      }
    }

    arbitrary_content_save_content($ac);
  }

  drupal_goto('admin/build/ac/' . $field->acname . '/fields');
}

function arbitrary_content_delete_field_confirm(&$form_state, $field) {
  $form_state['field'] = $field;
  $form_state['ac'] = arbitrary_content_load($field->acname);
  $form = array();

  $cancel = 'admin/build/ac/edit_fields/' . $field->acid;
  if (!empty($_REQUEST['cancel']))
    $cancel = $_REQUEST['cancel'];

  return confirm_form($form,
    t('Are you sure you want to delete the content %name?', array('%name' => $field->field_name)),
    $cancel,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

function arbitrary_content_delete_field_confirm_submit($form, &$form_state) {
  arbitrary_content_delete_field($form_state['ac'], $form_state['field']);
  drupal_set_message(t('The field has been deleted.'));
  $form_state['redirect'] = 'admin/build/ac/'. $form_state['ac']->name . '/fields';
}

function arbitrary_content_delete_confirm(&$form_state, $ac) {
  $form_state['acid'] = $ac->acid;
  $form = array();

  $cancel = 'admin/build/ac/list';
  if (!empty($_REQUEST['cancel']))
    $cancel = $_REQUEST['cancel'];

  return confirm_form($form,
    t('Are you sure you want to delete the content %name?', array('%name' => $ac->name)),
    $cancel,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

function arbitrary_content_delete_confirm_submit($form, &$form_state) {
  arbitrary_content_delete_content($form_state['acid']);
  drupal_set_message(t('The content has been deleted.'));
  $form_state['redirect'] = 'admin/build/ac/list';
}
